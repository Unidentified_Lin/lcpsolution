namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ST001", "CreateDateTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.ST001", "LastModifyTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ST001", "LastModifyTime");
            DropColumn("dbo.ST001", "CreateDateTime");
        }
    }
}
