namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNewTableMenuItem : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MenuItem",
                c => new
                    {
                        menuId = c.Guid(nullable: false),
                        actionName = c.String(),
                        controllerName = c.String(),
                        menuName = c.String(),
                        iconName = c.String(),
                        leftIndex = c.Int(nullable: false),
                        rightIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.menuId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MenuItem");
        }
    }
}
