﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Fund
{
    /// <summary>
    /// 代號設定檔
    /// </summary>
    public class ST001
    {
        [Key]
        [Column(Order = 0)]
        [DisplayName("流水號")]
        public Guid stId { get; set; }

        [DisplayName("設定代號")]
        public string stCode { get; set; }

        [DisplayName("設定名稱")]
        public string stName { get; set; }

        [DisplayName("建立時間")]
        public DateTime CreateDateTime { get; set; }

        [DisplayName("最後修改時間")]
        public DateTime LastModifyTime { get; set; }

    }
}