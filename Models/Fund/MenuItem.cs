﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Fund {
    public class MenuItem {
        [Key]
        [Column(Order = 0)]
        [DisplayName("流水號")]
        public Guid menuId { get; set; }

        [DisplayName("Action名稱")]
        public string actionName { get; set; }

        [DisplayName("Controller名稱")]
        public string controllerName { get; set; }

        [DisplayName("選單名稱")]
        public string menuName { get; set; }

        [DisplayName("選單圖示名稱")]
        public string iconName { get; set; }

        public int leftIndex { get; set; }

        public int rightIndex { get; set; }
    }
}
