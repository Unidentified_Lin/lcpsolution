﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Fund {
    /// <summary>
    /// 選單設定檔
    /// </summary>
    public class Menu {
        [Key]
        [Column(Order = 0)]
        [DisplayName("流水號")]
        public Guid menuId { get; set; }

        [DisplayName("Action名稱")]
        public string actionName { get; set; }

        [DisplayName("Controller名稱")]
        public string controllerName { get; set; }

        [DisplayName("選單名稱")]
        public string menuName { get; set; }

        [DisplayName("選單圖示名稱")]
        public string iconName { get; set; }

        [DisplayName("選單順序")]
        public int menuOrder { get; set; }

        [DisplayName("選單層級")]
        public int menuLevel { get; set; }

        //ForeignKey
        [DisplayName("父選單")]
        public Guid? parentMenuId { get; set; }

        [ForeignKey("parentMenuId")]
        public virtual Menu parentMenu { get; set; }

        public virtual ICollection<Menu> childrenMenu { get; set; }
    }
}
