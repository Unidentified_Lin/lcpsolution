﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Models.Fund {
    public class FundContext : DbContext {
        public FundContext() : base("name=FundContext") {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            //避免EF在產生Table名稱時自動變為複數
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<Models.Fund.ST001> ST001 { get; set; }
        public DbSet<Models.Fund.Menu> Menu { get; set; }
        public DbSet<Models.Fund.MenuItem> MenuItem { get; set; }
    }
}
