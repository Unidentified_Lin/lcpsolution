﻿using Models.Fund;

namespace WebApplication1.Models {
    public class MenuViewModel {
        public int firstOrder { get; set; }
        public int secondOrder { get; set; }
        public int thirdOrder { get; set; }
        public Menu Menu { get; set; }
    }
}