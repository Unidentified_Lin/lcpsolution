﻿using Models.Fund;
using System.Collections.Generic;
using System.Linq;
namespace WebApplication1.Models
{
    public class MenuItemModel
    {

        private FundContext db = new FundContext();

        private int theLevel = 0;
        private int theIndex = 1;

        public MenuItemModel()
        {
            DropDownMenus = new DropDownMenu();
        }

        public MenuItem menuItem { get; set; }

        /// <summary>
        /// 取項目序號
        /// </summary>
        /// <returns></returns>
        public int Index {
            get {
                //左index = 1 為 root
                if (menuItem.leftIndex == 1) {
                    return 1;
                }

                if (theIndex == 1) {
                    var result = from m in db.MenuItem
                                 where m.leftIndex <= menuItem.leftIndex
                                 select m;
                    Index = result.Count();
                }
                return Index;
            }
            set { theIndex = value; }
        }

        /// <summary>
        /// 取項目階層
        /// </summary>
        /// <returns></returns>
        public int Level {
            get {
                //左index = 1 為 root
                if (menuItem.leftIndex == 1) {
                    return 0;
                }

                if (theLevel == 0) {
                    //左index比我小以及右index比我大的個數
                    var parentsMenuItem = from m in db.MenuItem
                                          where m.leftIndex < menuItem.leftIndex && m.rightIndex > menuItem.rightIndex
                                          select m;
                    this.Level = parentsMenuItem.Count();
                }
                return theLevel;

            }
            set { theLevel = value; }
        }

        /// <summary>
        /// 取子項目個數
        /// </summary>
        /// <returns></returns>
        public int ChildCount()
        {
            //(右index - 左index - 1 ) / 2 個
            var result = menuItem.rightIndex - menuItem.leftIndex;
            if (result > 0) {
                return (result - 1) / 2;
            } else {
                return 0;
            }
        }

        /// <summary>
        /// 是否為末端項目
        /// </summary>
        /// <returns></returns>
        public bool IsEndNode()
        {
            return ChildCount() == 0 ? true : false;
        }

        public DropDownMenu DropDownMenus { get; set; }

        public class DropDownMenu
        {
            public void Add(MenuItemModel menuItem)
            {
                GetDropDownMenu.Add(menuItem);
            }

            public void AddRange(List<MenuItemModel> menuItems)
            {
                GetDropDownMenu.AddRange(menuItems);
            }

            public List<MenuItemModel> GetDropDownMenu { get; } = new List<MenuItemModel>();
        }

        /*
         *<li class="sidebar-heading">
         *  <span>actionName</span>
         *</li>
         */

        /*
         * <li class="sidebar-menu">
         *  <a href="controllerName/actionName">
         *      <span><i class="iconName"></i></span>
         *      linkText
         *  </a>
         * </li>
         */

        /*
         * <li class="sidebar-menu">
         *  <a href="#submenuId" data-toggle="collapse" class="dropdown-toggle">
         *      <span><i class="iconName"></i></span>
         *      linkText
         *  </a>
         *  <ul id="submenuId" class="list-unstyled collaspe" data-parent="#sidebar">
         *      <li><a href="#">linkText</a></li>
         *      <li><a href="#">linkText</a></li>
         *  </ul>
         * </li>
         */
    }
}