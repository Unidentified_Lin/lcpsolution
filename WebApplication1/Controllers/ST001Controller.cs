﻿using Models.Fund;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class ST001Controller : Controller
    {
        private FundContext db = new FundContext();

        public ActionResult Test() {
            return View();
        }

        // GET: ST001
        public ActionResult Index()
        {
            return View(db.ST001.ToList());
        }

        // GET: ST001/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ST001 sT001 = db.ST001.Find(id);
            if (sT001 == null) {
                return HttpNotFound();
            }
            return View(sT001);
        }

        // GET: ST001/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ST001/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "stId,stCode,stName")] ST001 sT001)
        {
            if (ModelState.IsValid) {
                DateTime currentTime = DateTime.Now;
                sT001.stId = Guid.NewGuid();
                sT001.CreateDateTime = currentTime;
                sT001.LastModifyTime = currentTime;
                db.ST001.Add(sT001);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sT001);
        }

        // GET: ST001/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ST001 sT001 = db.ST001.Find(id);
            if (sT001 == null) {
                return HttpNotFound();
            }
            return View(sT001);
        }

        // POST: ST001/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "stId,stCode,stName,CreateDateTime")] ST001 sT001)
        {
            if (ModelState.IsValid) {
                sT001.LastModifyTime = DateTime.Now;
                db.Entry(sT001).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sT001);
        }

        // GET: ST001/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ST001 sT001 = db.ST001.Find(id);
            if (sT001 == null) {
                return HttpNotFound();
            }
            return View(sT001);
        }

        // POST: ST001/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            ST001 sT001 = db.ST001.Find(id);
            db.ST001.Remove(sT001);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Search(string searchString)
        {
            var sT001s = from m in db.ST001 select m;
            if (!String.IsNullOrEmpty(searchString)) {
                sT001s = sT001s.Where(s => s.stCode.Contains(searchString));
            }
            return View("Index", sT001s);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
