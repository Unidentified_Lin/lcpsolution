﻿using Models.Fund;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class MenuController : Controller
    {
        private FundContext db = new FundContext();

        // 產生右側選單
        public ActionResult SidebarMenu()
        {
            var result = from m in db.Menu
                         join p in db.Menu on m.parentMenuId equals p.menuId into ps
                         from p in ps.DefaultIfEmpty()
                         select new MenuViewModel
                         {
                             firstOrder = m.parentMenuId.HasValue ? (p.parentMenuId.HasValue ? p.parentMenu.menuOrder : p.menuOrder) : m.menuOrder,
                             secondOrder = m.parentMenuId.HasValue ? (p.parentMenuId.HasValue ? p.menuOrder : m.menuOrder) : 0,
                             thirdOrder = m.parentMenuId.HasValue ? (p.parentMenuId.HasValue ? m.menuOrder : 0) : 0,
                             Menu = m
                         } into s
                         orderby s.firstOrder, s.secondOrder, s.thirdOrder
                         select s;

            foreach (var item in result) {
                Debug.WriteLine("First: " + item.firstOrder + " Second: " + item.secondOrder + " Third: " + item.thirdOrder + " name: " + item.Menu.menuName);
            }

            return PartialView("_SidebarMenu", result);
        }

        public ActionResult SidebarMenu2()
        {
            //產生選單測試區塊
            var menus = from m in db.MenuItem
                        select new MenuItemModel
                        {
                            menuItem = m
                        } into mi
                        orderby mi.menuItem.leftIndex
                        select mi;
            List<MenuItemModel> menuItems = menus.ToList();
            MenuItemModel poppedMenu = null;
            Stack<MenuItemModel> stackMenus = new Stack<MenuItemModel>();
            int level = 0;
            int preLeftIndex = 1;

            int totalCount = menuItems.Count();
            for (int i = 0; i < totalCount; i++) {
                MenuItemModel menuItem = menuItems.ElementAt(i);

                if (menuItem.menuItem.leftIndex == 1) {
                    //root
                    stackMenus.Push(menuItem);
                } else {
                    bool isEndNode = menuItem.IsEndNode();
                    int leftIndex = menuItem.menuItem.leftIndex;
                    int leftIndexRange = leftIndex - preLeftIndex;
                    preLeftIndex = leftIndex;

                    switch (leftIndexRange) {
                        case 1:
                            //this is subNode of pre (to next level)
                            level++;
                            if (!isEndNode) {
                                stackMenus.Push(menuItem);
                            } else {
                                if (stackMenus.Any()) {
                                    poppedMenu = stackMenus.Pop();
                                    poppedMenu.DropDownMenus.Add(menuItem);
                                }
                            }
                            break;
                        case 2:
                            //this is sibNode of pre (same level)
                            if (!isEndNode) {
                                stackMenus.Push(poppedMenu);
                                stackMenus.Push(menuItem);
                            } else {
                                poppedMenu.DropDownMenus.Add(menuItem);
                            }
                            break;
                        default:
                            //this is upper n-2 level sibNode of pre (to upper n-2 level)
                            int shiftLevel = leftIndexRange - 2;
                            level = level - shiftLevel;

                            for (int j = 0; j < shiftLevel; j++) {
                                MenuItemModel tempPoppedMenu = stackMenus.Pop();
                                tempPoppedMenu.DropDownMenus.Add(poppedMenu);
                                poppedMenu = tempPoppedMenu;
                            }

                            if (!isEndNode) {
                                stackMenus.Push(poppedMenu);
                                stackMenus.Push(menuItem);
                            } else {
                                poppedMenu.DropDownMenus.Add(menuItem);
                            }
                            break;
                    }
                    menuItem.Level = level;
                }

                //Last one
                if ((i + 1) == totalCount) {
                    while (stackMenus.Any()) {
                        MenuItemModel tempPoppedMenu = stackMenus.Pop();
                        if (poppedMenu != null) {
                            tempPoppedMenu.DropDownMenus.Add(poppedMenu);
                        }
                        poppedMenu = tempPoppedMenu;
                    }
                }
            }

            return PartialView("_SidebarMenu", poppedMenu);
        }

        // GET: Menu
        public ActionResult Index()
        {
            var result = from m in db.Menu
                         join p in db.Menu on m.parentMenuId equals p.menuId into ps
                         from p in ps.DefaultIfEmpty()
                         select new MenuViewModel
                         {
                             firstOrder = m.parentMenuId.HasValue ? (p.parentMenuId.HasValue ? p.parentMenu.menuOrder : p.menuOrder) : m.menuOrder,
                             secondOrder = m.parentMenuId.HasValue ? (p.parentMenuId.HasValue ? p.menuOrder : m.menuOrder) : 0,
                             thirdOrder = m.parentMenuId.HasValue ? (p.parentMenuId.HasValue ? m.menuOrder : 0) : 0,
                             Menu = m
                         } into s
                         orderby s.firstOrder, s.secondOrder, s.thirdOrder
                         select s.Menu;
            return View(result);
        }

        // GET: Menu/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Menu menu = db.Menu.Find(id);
            if (menu == null) {
                return HttpNotFound();
            }
            return View(menu);
        }

        // GET: Menu/Create
        public ActionResult Create()
        {
            Assembly asm = Assembly.GetAssembly(typeof(MvcApplication));

            var controlleractionlist = asm.GetTypes()
                    .Where(type => typeof(System.Web.Mvc.Controller).IsAssignableFrom(type))
                    .SelectMany(type => type.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public))
                    .Where(m => !m.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true).Any())
                    .Select(x => new { Controller = x.DeclaringType.Name, Action = x.Name, ReturnType = x.ReturnType.Name, Attributes = String.Join(",", x.GetCustomAttributes().Select(a => a.GetType().Name.Replace("Attribute", ""))) })
                    .OrderBy(x => x.Controller).ThenBy(x => x.Action).ToList();

            foreach (var item in controlleractionlist) {
                Debug.WriteLine("Controller: " + item.Controller + " Action: " + item.Action + " ReturnType: " + item.ReturnType + " Attributes: " + item.Attributes);
            }
            return View();
        }

        // POST: Menu/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "menuId,actionName,controllerName,menuName,menuLevel,menuOrder,iconName")] Menu menu)
        {
            if (ModelState.IsValid) {
                menu.menuId = Guid.NewGuid();
                db.Menu.Add(menu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(menu);
        }

        // GET: Menu/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Menu menu = db.Menu.Find(id);
            if (menu == null) {
                return HttpNotFound();
            }
            return View(menu);
        }

        // POST: Menu/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "menuId,actionName,controllerName,menuName,menuLevel,menuOrder,iconName")] Menu menu)
        {
            if (ModelState.IsValid) {
                db.Entry(menu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(menu);
        }

        // GET: Menu/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Menu menu = db.Menu.Find(id);
            if (menu == null) {
                return HttpNotFound();
            }
            return View(menu);
        }

        // POST: Menu/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Menu menu = db.Menu.Find(id);
            db.Menu.Remove(menu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
