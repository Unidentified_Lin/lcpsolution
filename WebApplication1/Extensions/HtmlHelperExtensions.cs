﻿using Models.Fund;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using WebApplication1.Models;

namespace WebApplication1.Extensions
{
    public static class HtmlHelperExtensions
    {

        public static MvcHtmlString Test<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression)
        {
            helper.EditorFor(expression);
            return null;
        }

        /// <summary>
        /// 選單區塊標題列
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="heading"></param>
        /// <returns></returns>
        public static MvcHtmlString SidebarHeading(this HtmlHelper helper, string heading)
        {
            return new MvcHtmlString(MenuExtensions.SetHeading(heading).ToString(TagRenderMode.Normal));
        }

        /// <summary>
        /// 一階選單
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="linkText"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <param name="iconName"></param>
        /// <returns></returns>
        public static MvcHtmlString SidebarMenuActionLink(this HtmlHelper helper, string linkText, string actionName, string controllerName, string iconName)
        {
            TagBuilder tag_li_menu = new TagBuilder("li");
            tag_li_menu.AddCssClass("sidebar-menu");

            //active selected menu
            MenuExtensions.SetLiActive(helper, tag_li_menu, actionName, controllerName);

            TagBuilder tag_anchor = MenuExtensions.SetMenu(helper, linkText, actionName, controllerName, iconName);
            tag_li_menu.InnerHtml = tag_anchor.ToString(TagRenderMode.Normal);
            return new MvcHtmlString(tag_li_menu.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString SidebarMenuActionLink(this HtmlHelper helper, IGrouping<int, MenuViewModel> subGroup)
        {

            MenuViewModel menuViewModel = subGroup.First();
            Menu menu = menuViewModel.Menu;

            TagBuilder tag_li = null;
            if (menuViewModel.secondOrder.Equals(0)) { //heading
                tag_li = MenuExtensions.SetHeading(menu.menuName);
            } else { //第一或第二階層
                TagBuilder tag_li_menu = new TagBuilder("li");
                tag_li_menu.AddCssClass("sidebar-menu");

                //active selected menu
                MenuExtensions.SetLiActive(helper, tag_li_menu, menu.actionName, menu.controllerName);

                bool hasChildern = menu.childrenMenu.Any();
                if (!hasChildern) { //第一階層
                    TagBuilder tag_anchor = MenuExtensions.SetMenu(helper, menu.menuName, menu.actionName, menu.controllerName, menu.iconName);
                    tag_li_menu.InnerHtml = tag_anchor.ToString(TagRenderMode.Normal);
                } else { //第二階層
                    string ulId = menu.menuName.Replace(" ", "").ToLower() + "Subment";
                    TagBuilder tag_anchor = MenuExtensions.SetMenu(helper, menu.menuName, ulId, "#", menu.iconName);

                    //第二階層Menus
                    TagBuilder tag_ul = new TagBuilder("ul");
                    tag_ul.GenerateId(ulId);
                    tag_ul.AddCssClass("collapse");
                    tag_ul.AddCssClass("list-unstyled");
                    tag_ul.MergeAttribute("data-parent", "#sidebar");

                    string tag_ul_inner = "";
                    foreach (MenuViewModel mvm in subGroup) {
                        Menu subMenuItem = mvm.Menu;
                        TagBuilder tag_li_sub = MenuExtensions.SetSubMenu(helper, subMenuItem.menuName, subMenuItem.actionName, subMenuItem.controllerName);
                        tag_ul_inner = tag_ul_inner + tag_li_sub.ToString(TagRenderMode.Normal);
                    }
                    tag_ul.InnerHtml = tag_ul_inner;

                    tag_li_menu.InnerHtml = tag_anchor.ToString(TagRenderMode.Normal) + tag_ul.ToString(TagRenderMode.Normal);
                }
                tag_li = tag_li_menu;
            }

            return new MvcHtmlString(tag_li.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString GenerateMenu(this HtmlHelper helper, MenuItemModel rootMenuItemModel)
        {
            foreach (MenuItemModel menuItem in rootMenuItemModel.DropDownMenus.GetDropDownMenu) {

            }
            return null;
        }

        private static void genMenu(this HtmlHelper helper, MenuItemModel menuItemModel)
        {
            MenuItem menuItem = menuItemModel.menuItem;
            switch (menuItemModel.Level) {
                case 1:
                    TagBuilder tg = MenuExtensions.SetHeading(menuItem.menuName);
                    break;
                case 2:
                    TagBuilder tg2 = MenuExtensions.SetMenu(helper, menuItem.menuName, menuItem.actionName, menuItem.controllerName, menuItem.iconName);
                    break;
                case 3:
                    TagBuilder tg3 = MenuExtensions.SetSubMenu(helper, menuItem.menuName, menuItem.actionName, menuItem.controllerName);
                    break;
                default:
                    break;
            }
        }

        //public static MvcHtmlString GenerateMenu(this HtmlHelper helper, IEnumerable<MenuViewModel> menuViewModels) {

        //    List<MenuItemExtensions> levelOneTemp = new List<MenuItemExtensions>();
        //    List<MenuItemExtensions> levelTwoTemp = new List<MenuItemExtensions>();
        //    List<MenuItemExtensions> levelThreeTemp = new List<MenuItemExtensions>();

        //    foreach (MenuViewModel mvm in menuViewModels) {

        //        int level = mvm.Menu.menuLevel;
        //        Menu menu = mvm.Menu;

        //        if (level == 0) {
        //            MenuItemExtensions menuItem = new MenuItemExtensions {
        //                linkText = menu.menuName,
        //                level = 0
        //            };

        //            if (levelThreeTemp.Count > 0) {
        //                levelTwoTemp.Reverse();
        //                menuItem.DropDownMenus.AddRange(levelTwoTemp.ToArray());
        //                levelTwoTemp.Clear();
        //            }
        //            levelOneTemp.Add(menuItem);
        //        }
        //        if (level == 1) {

        //            MenuItemExtensions menuItem = new MenuItemExtensions {
        //                linkText = menu.menuName,
        //                actionName = menu.actionName,
        //                controllerName = menu.controllerName,
        //                iconName = menu.iconName,
        //                level = 1
        //            };

        //            if (levelThreeTemp.Count > 0) {
        //                levelThreeTemp.Reverse();
        //                menuItem.DropDownMenus.AddRange(levelThreeTemp.ToArray());
        //                levelThreeTemp.Clear();


        //            }
        //            levelTwoTemp.Add(menuItem);
        //        }
        //        if (level == 2) {
        //            MenuItemExtensions menuItem = new MenuItemExtensions {
        //                linkText = menu.menuName,
        //                actionName = menu.actionName,
        //                controllerName = menu.controllerName,
        //                iconName = menu.iconName,
        //                level = 2
        //            };
        //            levelThreeTemp.Add(menuItem);
        //        }
        //    }
        //    levelOneTemp.Reverse();

        //    //TODO
        //    return new MvcHtmlString(""); ;
        //}
    }
}