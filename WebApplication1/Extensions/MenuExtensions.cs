﻿using System;
using System.Web.Mvc;

namespace WebApplication1.Extensions
{
    public static class MenuExtensions
    {

        /// <summary>
        /// 產生Heading的li標籤
        /// </summary>
        /// <param name="heading"></param>
        /// <returns></returns>
        public static TagBuilder SetHeading(string heading)
        {
            TagBuilder tag_li_menu = new TagBuilder("li");
            tag_li_menu.AddCssClass("sidebar-heading"); //<li class="sidebar-heading"></li>

            TagBuilder tag_span = new TagBuilder("span");
            tag_span.InnerHtml = heading; //<span>actionName</span>
            tag_li_menu.InnerHtml = tag_span.ToString(TagRenderMode.Normal);

            /*
             * <li class="sidebar-heading
             *   <span>actionName</span>
             * </li>
             */

            return tag_li_menu;
        }

        /// <summary>
        /// 產生icon標籤
        /// </summary>
        /// <param name="iconName"></param>
        /// <returns></returns>
        public static TagBuilder SetIcon(string iconName)
        {
            //set default icon
            if (string.IsNullOrEmpty(iconName)) {
                iconName = "smile";
            }

            TagBuilder tag_icon = new TagBuilder("i");
            //Font Awesome icon
            if (iconName.StartsWith("fas")) {
                //<i class="iconName"></i>
                tag_icon.AddCssClass(iconName);
            } else { //feather icon
                     //<i data-feather="iconName"></i>
                tag_icon.MergeAttribute("data-feather", iconName);
            }

            TagBuilder tag_span = new TagBuilder("span");
            tag_span.InnerHtml = tag_icon.ToString(TagRenderMode.Normal);

            /*
             * <span><i class="iconName"></i></span>
             */

            return tag_span;
        }

        /// <summary>
        /// 產生階層一Menu
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="linkText"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <param name="tag_icon"></param>
        /// <returns></returns>
        public static TagBuilder SetMenu(this HtmlHelper helper, string linkText, string actionName, string controllerName, string iconName)
        {
            //<a href="#"></a>
            //<a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
            TagBuilder tag_anchor = new TagBuilder("a");
            if (String.IsNullOrEmpty(actionName) || String.IsNullOrEmpty(controllerName) || "#".Equals(actionName) || "#".Equals(controllerName)) {
                if (actionName.EndsWith("Submenu")) {
                    tag_anchor.MergeAttribute("href", "#");
                } else {
                    tag_anchor.MergeAttribute("href", "#" + actionName);
                    tag_anchor.MergeAttribute("data-toggle", "collapse");
                    tag_anchor.MergeAttribute("aria-expanded", "false");
                    tag_anchor.AddCssClass("dropdown-toggle");
                }

            } else {
                tag_anchor.MergeAttribute("href", new UrlHelper(helper.ViewContext.RequestContext).Action(actionName, controllerName));
            }

            TagBuilder tag_icon = SetIcon(iconName);
            tag_anchor.InnerHtml = tag_icon.ToString(TagRenderMode.Normal) + linkText;

            /* 
             * <a href="#">
             *  <span><i class="iconName"></i></span>
             *  linkText
             * </a>
             */

            return tag_anchor;
        }

        /// <summary>
        /// 產生階層二Menu
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="linkText"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <returns></returns>
        public static TagBuilder SetSubMenu(this HtmlHelper helper, string linkText, string actionName, string controllerName)
        {
            TagBuilder tag_anchor = new TagBuilder("a");
            if (String.IsNullOrEmpty(actionName) || String.IsNullOrEmpty(controllerName) || "#".Equals(actionName) || "#".Equals(controllerName)) {
                tag_anchor.MergeAttribute("href", "#");
            } else {
                tag_anchor.MergeAttribute("href", new UrlHelper(helper.ViewContext.RequestContext).Action(actionName, controllerName));
            }
            tag_anchor.InnerHtml = linkText;

            TagBuilder tag_li = new TagBuilder("li");
            tag_li.InnerHtml = tag_anchor.ToString(TagRenderMode.Normal);

            /*
             * <li><a href="#">Page 1</a></li>
             */

            return tag_li;
        }

        public static TagBuilder SetEndMenu(this HtmlHelper helper, string linkText, string actionName, string controllerName, int level, string iconName)
        {
            TagBuilder tag_li = new TagBuilder("li");
            TagBuilder tag_anchor = new TagBuilder("a");
            TagBuilder tag_icon = null;

            if (!string.IsNullOrEmpty(iconName)) {
                tag_icon = SetIcon(iconName);
            }

            switch (level) {
                case 1: //heading
                    tag_li.AddCssClass("sidebar-heading");
                    break;
                case 2: //第一層
                    tag_li.AddCssClass("sidebar-menu");
                    break ;
                case 3: //第二層
                    break;
                case 4: //第三層
                    break;
                default:
                    break;
            }

            if (tag_icon != null) {
                linkText = tag_icon.ToString(TagRenderMode.Normal) + linkText;
            }

            if (String.IsNullOrEmpty(actionName) || String.IsNullOrEmpty(controllerName) || "#".Equals(actionName) || "#".Equals(controllerName)) {
                tag_anchor.MergeAttribute("href", "#");
            } else {
                tag_anchor.MergeAttribute("href", new UrlHelper(helper.ViewContext.RequestContext).Action(actionName, controllerName));
            }
            tag_anchor.InnerHtml = linkText;

            tag_li.InnerHtml = tag_anchor.ToString(TagRenderMode.Normal);

            /*
             * <li><a href="#">Page 1</a></li>
             */

            return tag_li;
        }

        /// <summary>
        /// active selected menu
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="li_tag"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        public static void SetLiActive(HtmlHelper helper, TagBuilder li_tag, string actionName, string controllerName)
        {
            string routCon = helper.ViewContext.ParentActionViewContext.RouteData.Values["controller"].ToString();
            string routAct = helper.ViewContext.ParentActionViewContext.RouteData.Values["action"].ToString();
            if (routCon.Equals(controllerName) && routAct.Equals(actionName)) {
                li_tag.AddCssClass("active");
            }
        }
    }
}